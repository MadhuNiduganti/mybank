package com.mybank.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="favourite_account")
public class FavouriteAccount {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer favouriteAccountId;
	private String accountName;
	private String accountNumber; 
	private Integer bankCode;
	private Integer customerId;
	private String accountStatus;
	
	
	public FavouriteAccount(Integer favouriteAccountId, String accountName, String accountNumber, Integer bankCode,
			Integer customerId, String accountStatus) {
		super();
		this.favouriteAccountId = favouriteAccountId;
		this.accountName = accountName;
		this.accountNumber = accountNumber;
		this.bankCode = bankCode;
		this.customerId = customerId;
		this.accountStatus = accountStatus;
	}

	public FavouriteAccount() {}

	public Integer getFavouriteAccountId() {
		return favouriteAccountId;
	}

	public void setFavouriteAccountId(Integer favouriteAccountId) {
		this.favouriteAccountId = favouriteAccountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Integer getBankCode() {
		return bankCode;
	}

	public void setBankCode(Integer bankCode) {
		this.bankCode = bankCode;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	
		
}
