package com.mybank.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="bankdetails")
public class BankDetails {

	private Integer bankCode;
	private String bankName;
	
	public BankDetails() {}
	
	public Integer getBankCode() {
		return bankCode;
	}
	public void setBankCode(Integer bankCode) {
		this.bankCode = bankCode;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public BankDetails(Integer bankCode, String bankName) {
		super();
		this.bankCode = bankCode;
		this.bankName = bankName;
	}
	
	
	
	
}
